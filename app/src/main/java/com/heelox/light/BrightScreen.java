package com.heelox.light;

import android.app.Activity;
import android.content.Context;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;


public class BrightScreen extends Activity {

    float screenBrightness = 0.5f;

    protected PowerManager.WakeLock mWakeLock;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bright_screen);

        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        setBright();
    }

    private void setBright() {
        WindowManager.LayoutParams layout = getWindow().getAttributes();
        //Making a copy so when the user exits we can set the previous value
        screenBrightness = layout.screenBrightness;
        layout.screenBrightness = 1f;
        getWindow().setAttributes(layout);

        this.mWakeLock.acquire();
    }

    @Override
   protected void onDestroy() {
        WindowManager.LayoutParams layout = getWindow().getAttributes();
        layout.screenBrightness = screenBrightness;
        getWindow().setAttributes(layout);
        this.mWakeLock.release();
        super.onDestroy();
    }

}
